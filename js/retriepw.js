function ps(){
    if (this.forms.password.type="password")
        box.innerHTML="<input type=\"html\" name=\"password\" size=\"20\" value="+this.forms.password.value+">";
    click.innerHTML="<a href=\"javascript:txt()\" class=\"iconfont icon-open-eye\"></a>"}
function txt(){
    if (this.forms.password.type="text")
        box.innerHTML="<input type=\"password\" name=\"password\" size=\"20\" value="+this.forms.password.value+">";
    click.innerHTML="<a href=\"javascript:ps()\" class=\"iconfont icon-biyan\"></a>"}
$(function(){
    $(window).resize();
});
//js设置居中
$(window).resize(function(){
    $(".login-contain").css({
        left: ($(window).width() - $(".login-contain").outerWidth())/2,
        top: ($(window).height() - $(".login-contain").outerHeight())/2
    });
});
/**
 * 验证码
 * @param {Object} o 验证码长度
 */
$.fn.code_Obj = function(o) {

    var _this = $(this);
    var options = {
        code_l: o.codeLength,//验证码长度
        codeChars: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        ],
        codeColors: ['#f44336', '#009688', '#cddc39', '#03a9f4', '#9c27b0', '#5e4444', '#9ebf9f', '#ffc8c4', '#2b4754', '#b4ced9', '#835f53', '#aa677e'],
        code_Init: function() {
            var code = "";
            var codeColor = "";
            var checkCode = _this.find("#data_code");
            for(var i = 0; i < this.code_l; i++) {
                var charNum = Math.floor(Math.random() * 52);
                code += this.codeChars[charNum];
            }
            for(var i = 0; i < this.codeColors.length; i++) {
                var charNum = Math.floor(Math.random() * 12);
                codeColor = this.codeColors[charNum];
            }
            console.log(code);
            if(checkCode) {
                checkCode.css('color', codeColor);
                checkCode.className = "code";
                checkCode.text(code);
                checkCode.attr('data-value', code);
            }
        }
    };

    options.code_Init();//初始化验证码
    _this.find("#data_code").bind('click', function() {
        options.code_Init();
    });
};
/**
 * 验证码
 * codeLength:验证码长度
 */
$('#Graphics').code_Obj({
    codeLength: 4
});
$("#Mailretrieval").click(function () {
    window.location.href="Mail-retrieval.html";
})