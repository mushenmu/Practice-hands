
$(function(){
    $(window).resize();
});
//js设置居中
$(window).resize(function(){

    $(".login-contain").css({
        left: ($(window).width() - $(".login-contain").outerWidth()) / 2,
        top: ($(window).height() - $(".login-contain").outerHeight()) / 2
    });
        var journalId;
        $(".checkboxwrappers").off('click','div').on("click", "div", function () {
            var i = $(this).find("ul").attr("i");
            if (i == 1) {
                $(this).find("ul").removeClass("active");
                $(this).find("ul").attr("i", "");
            } else {
                $(this).find("ul").addClass("active");
                $(this).find("ul").attr("i", 1);
            }
          /*  $(this).siblings("div").find("ul").removeClass("active");
            $(this).siblings("div").find("ul").attr("i", "");*/

            var address = $(this).find("ul").children("li").get(0);
            var journalIds = address.children[0];
            journalId = journalIds.getAttribute("i");


        });
    $(function () {
        var imgUploads=[];
         imgChangeone=function (obj1, obj2) {
             debugger
            var previewImgList = document.querySelector('.z_photo-one');

            var liLen = previewImgList.getElementsByTagName('div').length;

            //获取点击的文本框
            var file = document.getElementById("file-one");
            //存放图片的父级元素
            var imgContainer = document.getElementsByClassName(obj1)[0];
            //获取的图片文件
            var fileList = file.files;
            var imgLen = fileList.length;
            var ImgLen = imgLen + liLen ;
            if(ImgLen > 8){
                alert("上传图片最大数量不能超过7张");
                return false;
            }
            //文本框的父级元素
            var input = document.getElementsByClassName(obj2)[0];
            var imgArr = [];
            //遍历获取到得图片文件
            for (var i = 0; i < fileList.length; i++) {
                var formData = new FormData();
                var file = fileList[i];
                var matterimgId;
                formData.append("file", file);
                $.ajax({
                    type: "POST",
                    url: path()+"/matterFile/upload/" + 1,
                    data: formData,
                    processData: false,//这个很有必要，不然不行
                    contentType: false,
                    dataType: "json",
                    mimeType: "multipart/form-data",
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function (data) {
                        if (data.code != 1000) {
                            jqalert({
                                title:'提示',
                                content:'上传失败'
                            })
                        } else {
                            imgUploads.push(data.data);
                            matterimgId=data.data;
                            alert("上传成功");
                            /*    $("#img").attr('src',);*/
                        };

                    }


                });
                var imgUrl = window.URL.createObjectURL(file.files[i]);
                imgArr.push(imgUrl);
                var img = document.createElement("img");
                img.setAttribute("src", imgArr[i]);
                var imgAdd = document.createElement("div");
                imgAdd.setAttribute("class", "z_addImg");
                imgAdd.setAttribute("i", matterimgId);
                imgAdd.appendChild(img);
                imgContainer.appendChild(imgAdd);
                //获取的图片文件
                // 上传图片


            };
            imgRemoveone();
        };


        function imgRemoveone() {
            debugger;
            var imgList = document.getElementsByClassName("z_addImg");
            var mask = document.getElementsByClassName("z_mask-one")[0];
            var cancel = document.getElementsByClassName("z_cancel-one")[0];
            var sure = document.getElementsByClassName("z_sure-one")[0];
            for (var j = 0; j < imgList.length; j++) {
                imgList[j].index = j;
                imgList[j].onclick = function() {
                    var t = this;
                    mask.style.display = "block";
                    cancel.onclick = function() {
                        mask.style.display = "none";
                    };
                    sure.onclick = function() {
                        mask.style.display = "none";
                        var obj=   t.parentNode;
                        var mtimgId=  t. getAttribute("i");
                        $.ajax({
                            type: "GET",
                            url: path()+"/matterFile/remove/" + mtimgId,
                            dataType: "json",
                            contentType: "application/json",
                            xhrFields: {
                                withCredentials: true
                            },
                            crossDomain: true,
                            success: function (data) {
                                if (data.code != 1000) {
                                    jqalert({
                                        title: '提示',
                                        content: '删除失败'
                                    })
                                } else if (data.code == 1000) {
                                    {

                                        obj.removeChild(t);
                                        jqalert({
                                            title: '提示',
                                            content: '删除成功'
                                        })
                                        /*    $("#img").attr('src',);*/
                                    }
                                    ;

                                }
                            }


                        });
                    };

                }
            };
        };

        layui.use(['form','laydate'], function(){
            var form = layui.form
                ,layer = layui.layer
                ,layedit = layui.layedit
                ,laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#date1' //指定元素
            });
            //监听提交
            form.on('submit(formDemoone)', function(rel){
                var data = {} ;
                var  journalIds=[];
                var customerId =$("[name='customerId']").val();
                var matterName =  $("[name='matterName']").val();
                var payType =  $("[name='payType']").val();
                var payMoney =  $("[name='payMoney']").val();
                var serviceUserId =   $("[name='serviceUserId']").val();
                var time  =   $("[name='time']").val();
                var remark =   $("[name='remark']").val();
                data["customerId"] =customerId;
                data["matterName"] = matterName;
                data["payType"] = payType;
                data["payMoney"] = payMoney;
                data["serviceUserId"] =serviceUserId;
                data["time"] =time;
                data["remark"] =remark;
                data["fileIds"] = imgUploads;
                data["journalIds"] = journalIds;
                $.ajax({
                    type: "post",
                    url:path()+ "/matter/matter/add",
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    dataType:'json',
                    contentType: "application/json",
                    data:JSON.stringify(data),
                    success: function (data) {
                        console.log(data);
                        if (data.code!= 1000) {
                            alert("新增失败");
                        } else {
                            window.location.reload();
                            alert("新增成功")
                        }
                    }
                });
                return false;
            });

            $.ajax({
                type: 'get',
                url: path()+ '/customer/customer',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                scriptCharset: 'utf-8',
                success : function (data) {

                    if (data.code == 2004) {
                        alert("非法登陆");
                        location.href = "../web/Backup/login.html";
                    } else {

                        //接收返回的数据data
                        var $a = data;
                        var i = $a.data; 	//获取具体的参数
                        $b = {warn: []};
                        var arr = [];
                        for (var x = 1; x < i.length; x++) {
                            var serviceid = $a.data[x].servicerId;

                            if (arr.indexOf(serviceid) == -1) {
                                //  -1代表没有找到
                                arr.push($a.data[x].servicerId);
                                var sel = $a.data[x]//如果没有找到就把这个name放到arr里面，以便下次循环时用
                                $b.warn.push(sel);
                            }
                        }
                        var newarr = $b;
                        num = $b.warn.length;       //去重后的数组
                        for (j = 0; j < num; j++) {
                            var pd = $b.warn[j];

                            $("#servicerId").append(' <option value="' + pd.servicerId + '">' + pd.servicerName + '</option>');
                        }
                        form.render('select');
                    }
                },
            });
            $.ajax({
                type: 'get',
                url: path()+ '/customer/customer',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                scriptCharset: 'utf-8',
                success: function (data) {


                    for (var i = 0; i < data.data.length; i++) {
                        var pd=data.data[i];
                        /*  $("#demo_select1").append('<li i="'+pd.customerId+'" ><a href="javaScript:">'+pd.customerName+'</a></li>');*/
                        $("#customer").append(' <option value="'+pd.customerId+'">'+pd.customerName+'</option>');

                    }
                    form.render('select');

                }

            });

            $.ajax({
                type: 'get',
                url: path()+ '/payMethodController/payMethod',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                scriptCharset: 'utf-8',
                success: function (data) {


                    for (var i = 0; i < data.data.length; i++) {
                        var pd=data.data[i];
                        /*  $("#demo_select1").append('<li i="'+pd.customerId+'" ><a href="javaScript:">'+pd.customerName+'</a></li>');*/
                        $("#payMethod").append(' <option value="'+pd.payMethodId+'">'+pd.payMethodName+'</option>');

                    }
                    form.render('select');

                }

            });
        });


    })





});


$("#button_tp").click(function () {
    window.location.href="add-ctures.html"
})