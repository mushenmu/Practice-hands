$(function(){
    $(window).resize();
});

//accounting
//js设置居中
$(window).resize(function(){
    $(".login-contain").css({
        left: ($(window).width() - $(".login-contain").outerWidth())/2,
        top: ($(window).height() - $(".login-contain").outerHeight())/2
    });
});
function go(x, i) {
    switch (x) {
        case 1:
            window.location.href = "index.html";
            break;
        case 2:
            if (i === 0) {
                window.location.href = "../submission/index-demo.html";
            }
            else {
                window.location.href = "../submission/index-demo.html";
            }
            break;
        case 3:
            if (i === 0) {

                window.location.href = "../admin/user_admin.html";
            }
            else {


                window.location.href = "../admin/user_admin.html" ;
            }
            break;
    }
}


$("#g2").click(function () {
    window.location.href="../Approval/index-Review.html";
})
$("#info1").click(function () {
    window.location.href="../Cashier/index-Income-breakdown.html";
    // window.open("../daily/index-Income-breakdown.html");
})
$("#info2").click(function () {
    window.location.href="../Cashier/index-Details-expenditures.html";
    // window.open("../daily/index-Details-expenditures.html");

})
$("#AdvanceReceipt").click(function () {
    // window.open("../daily/index-sz-Detail.html");
    window.location.href="../Cashier/index-Advance-receipt.html";
});
$("#Prepaid").click(function () {
    // window.open("../daily/index-sz-Detail.html");
    window.location.href="../Cashier/index-Prepaid.html";
});
$("#info4").click(function () {
    // window.open("../daily/index-sz-Detail.html");
    window.location.href="../Receivable/Receivable.html";
});
$("#info-income").click(function () {
    // window.open("../daily/index-sz-Detail.html");
    window.location.href="../accounting/Accounting-income.html";
});
$("#info-expenditure").click(function () {
    // window.open("../daily/index-sz-Detail.html");
    window.location.href="../accounting/Accounting-expenses.html";
});

$("#info5").click(function () {
    // window.open("../daily/index-sz-Detail.html");
    window.location.href="../Receivable/Handle.html";
});


$(document).ready(function () {
var data={};
data["customerId"]="";
data["endDate"]="";
data["endDay"]="";
  data["serviceUserId"]="";
  data["startDate"]="";
  data["startDay"]="";
    $.ajax({
        type:"POST",
        url:path()+"/chart/chart",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        scriptCharset: 'utf-8',
        contentType: "application/json",
        data:JSON.stringify(data),
        success: function (data) {
       var income=data.data.pie[0].money;
       var expenditure=data.data.pie[1].money;
       var profit=    income-expenditure;
       var total={};
            total["income"]=income;
            total["expenditure"]=expenditure;
            total["profit"]=profit;
            sessionStorage.setItem("total", JSON.stringify(total));



            console.log(data);

            if (data.code!=1000){
                alert("请求合计失败")
            }else{
                // 利润
                $(".profit").html(profit);
                $(".profit").val(profit);

                // 收入
                $(".income").html(income);
                $(".income").val(income);
                // 支出
                $(".expenditure").html(expenditure);
                $(".expenditure").val(expenditure);

            }

        }
    });
    $.ajax({
        type:"get",
        url:path()+"/user/role",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        scriptCharset: 'utf-8',
        contentType: "application/json",
        success: function (data) {
                   var role=             data.data
            console.log(data);
            if (role.length==1){
                $(".datawrap2 .list-two .button").css("width","45%");
                $(".datawrap2 .list-two .button").css("padding","3% 0;");
                $(".datawrap2 .list-two .ton").css("display","none");

            }
            sessionStorage.setItem("role", JSON.stringify(role));

        }
    });

    $(document).ready(function () {

        $.ajax({
            method: "get",
            url: path()+"/menu/menu",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (data) {
                sessionStorage.setItem("menu",JSON.stringify(data.data));
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].menuId == 8) {
                        $("#g2").css("display", "block");
                        $(".g1").css("width", "70%");

                    }
                }

            }
        });
    })
});

<!--出纳客户下拉-->
var search = {

    searchKeyword: function () {
        var nWord = $("#search-input").val();
        //var temarray = nWord.split(""); //分割
        var array = this.unique(nWord.split(""));
        var dsa = $("#search").find("ul li a");//获取全部列表
        var linumber = 0;

        $("#search ul li").show();
        for (var t = 0; t < dsa.length; t++) {
            $(dsa[t]).html($(dsa[t]).text());
            var temstr = ($(dsa[t]).text()).split("");
            var yes = false;
            for (var i = 0; i < array.length; i++) {
                var posarr = this.findAll(temstr, array[i]);
                if (posarr.length > 0) {
                    yes = true;
                    for (var j = 0; j < posarr.length; j++) {
                        temstr[posarr[j]] = "<em style='color:red;'>" + temstr[posarr[j]] + "</em>";
                    }
                }
            }
            if (!yes) {
                $(dsa[t]).closest("li").hide();
            }
            else {
                linumber++;
                var htmlstr = "";
                for (var m = 0; m < temstr.length; m++) {
                    htmlstr += temstr[m];
                }
                $(dsa[t]).html(htmlstr);
            }

        }
        if (linumber == 0) {
            $("#search ul li").show();
            $("#search ul").slideDown(200);
        }
    },
    findAll: function (arr, str) {

        var results = [],
            len = arr.length,
            pos = 0;
        while (pos < len) {
            pos = arr.indexOf(str, pos);
            if (pos === -1) {
                break;
            }
            results.push(pos);
            pos++;
        }
        return results;
    },
    unique: function (arr) {

        var new_arr = [];
        for (var i = 0; i < arr.length; i++) {
            var items = arr[i];
            //判断元素是否存在于new_arr中，如果不存在则插入到new_arr的最后
            if ($.inArray(items, new_arr) == -1) {
                new_arr.push(items);
            }
        }
        return new_arr;
    },
    changeValue: function (obj) {

        $('.dropdown ul').slideUp(200);
        var input = $(obj).find('.dropdown-selected');
        var ul = $(obj).find('ul');
        if (!ul.is(':visible')) {
            ul.slideDown('fast');
        } else {
            ul.slideUp('fast');
        }

        $(obj).find('ul a').click(function () {

            var customerId = $(this).attr("i");
            input.attr("i", customerId);
            input.val($(this).text());
            $(this).parent().addClass('active');
            $(this).parent().siblings().removeClass('active')
            $(this).closest('ul').slideUp(200);
            return false;
        })
        var e = this.getEvent();
        window.event ? e.cancelBubble = true : e.stopPropagation();
    },
    _init: function () {

        $("#search").on("click", "ul li a", function () {


            $("#search-input").val($(this).text());
            $(this).parent().addClass('active');
            $(this).parent().siblings().removeClass('active')
            $(this).closest('ul').slideUp(200);
            return false;
        })
    },
    getEvent: function () {


        if (window.event) {
            return window.event;
        }
        var f = arguments.callee.caller;
        do {
            var e = f.arguments[0];
            if (e && (e.constructor === Event || e.constructor === MouseEvent || e.constructor === KeyboardEvent)) {
                return e;
            }
        } while (f = f.caller);
    }

}
search._init();
<!--出纳服务人员下拉-->
    var searchtwo = {
        searchKeywordtwo: function () {
            var nWord = $("#search-input-two").val();
            //var temarray = nWord.split(""); //分割
            var array = this.unique(nWord.split(""));
            var dsa = $("#search-two").find("ul li a");//获取全部列表
            var linumber = 0;

            $("#search-two ul li").show();
            for (var t = 0; t < dsa.length; t++) {
                $(dsa[t]).html($(dsa[t]).text());
                var temstr = ($(dsa[t]).text()).split("");
                var yes = false;
                for (var i = 0; i < array.length; i++) {
                    var posarr = this.findAll(temstr, array[i]);
                    if (posarr.length > 0) {
                        yes = true;
                        for (var j = 0; j < posarr.length; j++) {
                            temstr[posarr[j]] = "<em style='color:red;'>" + temstr[posarr[j]] + "</em>";
                        }
                    }
                }
                if (!yes) {
                    $(dsa[t]).closest("li").hide();
                }
                else {
                    linumber++;
                    var htmlstr = "";
                    for (var m = 0; m < temstr.length; m++) {
                        htmlstr += temstr[m];
                    }
                    $(dsa[t]).html(htmlstr);
                }

            }
            if (linumber == 0) {
                $("#search-two ul li").show();
                $("#search-two ul").slideDown(200);
            }
        },
        findAll: function (arr, str) {

            var results = [],
                len = arr.length,
                pos = 0;
            while (pos < len) {
                pos = arr.indexOf(str, pos);
                if (pos === -1) {
                    break;
                }
                results.push(pos);
                pos++;
            }
            return results;
        },
        unique: function (arr) {

            var new_arr = [];
            for (var i = 0; i < arr.length; i++) {
                var items = arr[i];
                //判断元素是否存在于new_arr中，如果不存在则插入到new_arr的最后
                if ($.inArray(items, new_arr) == -1) {
                    new_arr.push(items);
                }
            }
            return new_arr;
        },
        changeValuetwo: function (obj) {

            $('.dropdown-two ul').slideUp(200);
            var input = $(obj).find('.dropdown-selected-two');
            var ul = $(obj).find('ul');
            if (!ul.is(':visible')) {
                ul.slideDown('fast');
            } else {
                ul.slideUp('fast');
            }

            $(obj).find('ul a').click(function () {


                var customerId = $(this).attr("i");
                input.attr("i", customerId);
                input.val($(this).text());
                var liandong = sessionStorage.getItem("liandong");
                var servicerId = JSON.parse(liandong);
                var i = servicerId;//获取具体的参数
                $b = {warn: []};
                var arr = [];
                for (var x = 1; x < i.length; x++) {
                    var serviceid = i[x].servicerId;

                    if (arr.indexOf(serviceid) == -1) {
                        //  -1代表没有找到
                        arr.push(i[x].servicerId);
                        var sel = i[x]//如果没有找到就把这个name放到arr里面，以便下次循环时用
                        $b.warn.push(sel);
                    }
                }
                var newarr = $b;
                num = $b.warn.length;       //去重后的数组
                for (j = 0; j < num; j++) {
                    var pd = $b.warn[j];
                    if (customerId == pd.servicerId) {
                        var customer = $('.dropdown-selected');
                        customer.attr("i", pd.customerId);
                        customer.val(pd.customerName);
                    }

                }

                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
            var e = this.getEvent();
            window.event ? e.cancelBubble = true : e.stopPropagation();
        },
        _init: function () {

            $("#search-two").on("click", "ul li a", function () {

                $("#search-input-two").val($(this).text());
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
        },
        getEvent: function () {


            if (window.event) {
                return window.event;
            }
            var f = arguments.callee.caller;
            do {
                var e = f.arguments[0];
                if (e && (e.constructor === Event || e.constructor === MouseEvent || e.constructor === KeyboardEvent)) {
                    return e;
                }
            } while (f = f.caller);
        }

    }
    searchtwo._init();
<!--会计服务人员下拉-->
    var searchthree = {
        searchKeywordthree: function () {
            var nWord = $("#search-input-three").val();
            //var temarray = nWord.split(""); //分割
            var array = this.unique(nWord.split(""));
            var dsa = $("#search-three").find("ul li a");//获取全部列表
            var linumber = 0;

            $("#search-three ul li").show();
            for (var t = 0; t < dsa.length; t++) {
                $(dsa[t]).html($(dsa[t]).text());
                var temstr = ($(dsa[t]).text()).split("");
                var yes = false;
                for (var i = 0; i < array.length; i++) {
                    var posarr = this.findAll(temstr, array[i]);
                    if (posarr.length > 0) {
                        yes = true;
                        for (var j = 0; j < posarr.length; j++) {
                            temstr[posarr[j]] = "<em style='color:red;'>" + temstr[posarr[j]] + "</em>";
                        }
                    }
                }
                if (!yes) {
                    $(dsa[t]).closest("li").hide();
                }
                else {
                    linumber++;
                    var htmlstr = "";
                    for (var m = 0; m < temstr.length; m++) {
                        htmlstr += temstr[m];
                    }
                    $(dsa[t]).html(htmlstr);
                }

            }
            if (linumber == 0) {
                $("#search-three ul li").show();
                $("#search-three ul").slideDown(200);
            }
        },
        findAll: function (arr, str) {

            var results = [],
                len = arr.length,
                pos = 0;
            while (pos < len) {
                pos = arr.indexOf(str, pos);
                if (pos === -1) {
                    break;
                }
                results.push(pos);
                pos++;
            }
            return results;
        },
        unique: function (arr) {

            var new_arr = [];
            for (var i = 0; i < arr.length; i++) {
                var items = arr[i];
                //判断元素是否存在于new_arr中，如果不存在则插入到new_arr的最后
                if ($.inArray(items, new_arr) == -1) {
                    new_arr.push(items);
                }
            }
            return new_arr;
        },
        changeValuethree: function (obj) {

            $('.dropdown-three ul').slideUp(200);
            var input = $(obj).find('.dropdown-selected-three');
            var ul = $(obj).find('ul');
            if (!ul.is(':visible')) {
                ul.slideDown('fast');
            } else {
                ul.slideUp('fast');
            }

            $(obj).find('ul a').click(function () {


                var customerId = $(this).attr("i");
                input.attr("i", customerId);
                input.val($(this).text());
                var liandong = sessionStorage.getItem("liandong");
                var servicerId = JSON.parse(liandong);
                var i = servicerId;//获取具体的参数
                $b = {warn: []};
                var arr = [];
                for (var x = 1; x < i.length; x++) {
                    var serviceid = i[x].servicerId;

                    if (arr.indexOf(serviceid) == -1) {
                        //  -1代表没有找到
                        arr.push(i[x].servicerId);
                        var sel = i[x]//如果没有找到就把这个name放到arr里面，以便下次循环时用
                        $b.warn.push(sel);
                    }
                }
                var newarr = $b;
                num = $b.warn.length;       //去重后的数组
                for (j = 0; j < num; j++) {
                    var pd = $b.warn[j];
                    if (customerId == pd.servicerId) {
                        var customer = $('.dropdown-selected');
                        customer.attr("i", pd.customerId);
                        customer.val(pd.customerName);
                    }

                }

                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
            var e = this.getEvent();
            window.event ? e.cancelBubble = true : e.stopPropagation();
        },
        _init: function () {

            $("#search-three").on("click", "ul li a", function () {

                $("#search-input-two").val($(this).text());
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
        },
        getEvent: function () {


            if (window.event) {
                return window.event;
            }
            var f = arguments.callee.caller;
            do {
                var e = f.arguments[0];
                if (e && (e.constructor === Event || e.constructor === MouseEvent || e.constructor === KeyboardEvent)) {
                    return e;
                }
            } while (f = f.caller);
        }

    }
    searchthree._init();
<!--会计客户下拉-->
    var searchfroe = {
        searchKeywordfroe: function () {
            var nWord = $("#search-input-fore").val();
            //var temarray = nWord.split(""); //分割
            var array = this.unique(nWord.split(""));
            var dsa = $("#search-froe").find("ul li a");//获取全部列表
            var linumber = 0;

            $("#search-froe ul li").show();
            for (var t = 0; t < dsa.length; t++) {
                $(dsa[t]).html($(dsa[t]).text());
                var temstr = ($(dsa[t]).text()).split("");
                var yes = false;
                for (var i = 0; i < array.length; i++) {
                    var posarr = this.findAll(temstr, array[i]);
                    if (posarr.length > 0) {
                        yes = true;
                        for (var j = 0; j < posarr.length; j++) {
                            temstr[posarr[j]] = "<em style='color:red;'>" + temstr[posarr[j]] + "</em>";
                        }
                    }
                }
                if (!yes) {
                    $(dsa[t]).closest("li").hide();
                }
                else {
                    linumber++;
                    var htmlstr = "";
                    for (var m = 0; m < temstr.length; m++) {
                        htmlstr += temstr[m];
                    }
                    $(dsa[t]).html(htmlstr);
                }

            }
            if (linumber == 0) {
                $("#search-froe ul li").show();
                $("#search-froe ul").slideDown(200);
            }
        },
        findAll: function (arr, str) {

            var results = [],
                len = arr.length,
                pos = 0;
            while (pos < len) {
                pos = arr.indexOf(str, pos);
                if (pos === -1) {
                    break;
                }
                results.push(pos);
                pos++;
            }
            return results;
        },
        unique: function (arr) {

            var new_arr = [];
            for (var i = 0; i < arr.length; i++) {
                var items = arr[i];
                //判断元素是否存在于new_arr中，如果不存在则插入到new_arr的最后
                if ($.inArray(items, new_arr) == -1) {
                    new_arr.push(items);
                }
            }
            return new_arr;
        },
        changeValuefroe: function (obj) {

            $('.dropdown-fore ul').slideUp(200);
            var input = $(obj).find('.dropdown-selected-fore');
            var ul = $(obj).find('ul');
            if (!ul.is(':visible')) {
                ul.slideDown('fast');
            } else {
                ul.slideUp('fast');
            }

            $(obj).find('ul a').click(function () {

                var customerId = $(this).attr("i");
                input.attr("i", customerId);
                input.val($(this).text());
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
            var e = this.getEvent();
            window.event ? e.cancelBubble = true : e.stopPropagation();
        },
        _init: function () {

            $("#search-froe").on("click", "ul li a", function () {


                $("#search-input-fore").val($(this).text());
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
        },
        getEvent: function () {


            if (window.event) {
                return window.event;
            }
            var f = arguments.callee.caller;
            do {
                var e = f.arguments[0];
                if (e && (e.constructor === Event || e.constructor === MouseEvent || e.constructor === KeyboardEvent)) {
                    return e;
                }
            } while (f = f.caller);
        }

    }
    searchfroe._init();
// <!--会计收支事项下拉-->
    var searchfif = {
        searchKeywordfif: function () {
            var nWord = $("#search-input-fif").val();
            //var temarray = nWord.split(""); //分割
            var array = this.unique(nWord.split(""));
            var dsa = $("#search-froe").find("ul li a");//获取全部列表
            var linumber = 0;

            $("#search-fif ul li").show();
            for (var t = 0; t < dsa.length; t++) {
                $(dsa[t]).html($(dsa[t]).text());
                var temstr = ($(dsa[t]).text()).split("");
                var yes = false;
                for (var i = 0; i < array.length; i++) {
                    var posarr = this.findAll(temstr, array[i]);
                    if (posarr.length > 0) {
                        yes = true;
                        for (var j = 0; j < posarr.length; j++) {
                            temstr[posarr[j]] = "<em style='color:red;'>" + temstr[posarr[j]] + "</em>";
                        }
                    }
                }
                if (!yes) {
                    $(dsa[t]).closest("li").hide();
                }
                else {
                    linumber++;
                    var htmlstr = "";
                    for (var m = 0; m < temstr.length; m++) {
                        htmlstr += temstr[m];
                    }
                    $(dsa[t]).html(htmlstr);
                }

            }
            if (linumber == 0) {
                $("#search-fif ul li").show();
                $("#search-fif ul").slideDown(200);
            }
        },
        findAll: function (arr, str) {

            var results = [],
                len = arr.length,
                pos = 0;
            while (pos < len) {
                pos = arr.indexOf(str, pos);
                if (pos === -1) {
                    break;
                }
                results.push(pos);
                pos++;
            }
            return results;
        },
        unique: function (arr) {

            var new_arr = [];
            for (var i = 0; i < arr.length; i++) {
                var items = arr[i];
                //判断元素是否存在于new_arr中，如果不存在则插入到new_arr的最后
                if ($.inArray(items, new_arr) == -1) {
                    new_arr.push(items);
                }
            }
            return new_arr;
        },
        changeValuefif: function (obj) {

            $('.dropdown-fif ul').slideUp(200);
            var input = $(obj).find('.dropdown-selected-fif');
            var ul = $(obj).find('ul');
            if (!ul.is(':visible')) {
                ul.slideDown('fast');
            } else {
                ul.slideUp('fast');
            }

            $(obj).find('ul a').click(function () {

                var customerId = $(this).attr("i");
                input.attr("i", customerId);
                input.val($(this).text());
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
            var e = this.getEvent();
            window.event ? e.cancelBubble = true : e.stopPropagation();
        },
        _init: function () {

            $("#search-fif").on("click", "ul li a", function () {


                $("#search-input-fif").val($(this).text());
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active')
                $(this).closest('ul').slideUp(200);
                return false;
            })
        },
        getEvent: function () {


            if (window.event) {
                return window.event;
            }
            var f = arguments.callee.caller;
            do {
                var e = f.arguments[0];
                if (e && (e.constructor === Event || e.constructor === MouseEvent || e.constructor === KeyboardEvent)) {
                    return e;
                }
            } while (f = f.caller);
        }

    }
    searchfif._init();
<!--搜索重置-->



    var startDate;
    var startsDate;
    var endDate;
    var endsDate;
    // 出纳时间类型
        var timeType;//1.payTime 支付时间 2.creatTime 提交时间 3.passTime 审批时间
    $(".list-two input").click(function () {

    var value = $(this).attr("i");
    if (timeType != value) {
    //更改列表条件
    timeType = value;
    $(".list-two .but").removeClass("but");
    $(this).addClass("but");
    var i=  $("#search-input").val();
    //重置列表数据

}
});
        var timeTypes; //1.time 所属时间  2.creatTime 提交时间  3.passTime  审批时间
    // 会计时间类型
    $(".list-twos input").click(function () {
        debugger

    var value = $(this).attr("i");
    if (timeTypes != value) {
    //更改列表条件
    timeTypes = value;
    $(".list-twos .but").removeClass("but");
    $(this).addClass("but");
    //重置列表数
}
});
    // 出纳判断
    function cgetclass() {
    var stdata=  $("#startDate").val();
    var endata=  $("#endDate").val();

    if (!stdata==""|| !endata=="") {
    if (!!timeType) {
    return true;
}else {
    return false;
}
}else {
    return true;
}

}
//会计判断
    function kgetclass() {
    var stdata=  $("#startsDate").val();
    var endata=  $("#endsDate").val();

    if (!stdata==""|| !endata=="") {
    if (!!timeTypes) {
    return true;
}else {
    return false;
}
}else {
    return true;
}

}
    // 过去一年时间
    function getPassYearFormatDate() {
    var nowDate = new Date();
    var date = new Date(nowDate);
    date.setDate(date.getDate()-365);
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
    month = "0" + month;
}
    if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
}
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate
}
    // 当前时间
    function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
    month = "0" + month;
}
    if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
}
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}
    // 出纳获取数据
    function Cashier() {
    var CustomerId= $("#search-input").attr("i");
    var serviceUserId= $("#search-input-two").attr("i");

    var customerId=CustomerId;

    var stdata=  $("#startDate").val();
    var endata=  $("#endDate").val();
    if (stdata==""){
    startDate=getPassYearFormatDate();
}else {
    startDate=stdata;
}
    if (endata==""){
    endDate=getNowFormatDate();
}else {

    endDate=endata;
}

    var data = {};
    data["pageNo"] = 1;
    data["pageSize"] = 10000000;
    data["timeType"] = timeType;
    data["startDate"] = startDate;
    data["endDate"] = endDate;
    data["payType"] = '';
    data["customerId"] =customerId;
    data["serviceUserId"] =serviceUserId;
    data["order"] = '';
    data["column"] = '';
        sessionStorage.setItem("daily", JSON.stringify(data));
    $.ajax({
    type: 'post',
    // url: 'js/pdlist1.json',
    url: path()+'/journal/journal',
    dataType: 'json',
    xhrFields: {
    withCredentials: true
},
    crossDomain: true,
    scriptCharset: 'utf-8',
    contentType: "application/json",
    data:JSON.stringify(data),
    success: function(data){

    var rel= data.data.data;
    // 收入
    var incomes=0;
    // 支出
    var expenditures=0;
    var Prepaid=0;
    var AdvanceReceipt=0;

    for (var i = 0; i < rel.length; i++) {

    var pd= rel[i];
    if (pd.payType == 1) {
    var   incom=pd.payMoney;
    incomes=incom+incomes;
}else if (pd.payType ==0){
    var   expen=pd.payMoney;
    expenditures=expen+expenditures;
}else if (pd.payType ==2){
        var   Prepa=pd.payMoney;
        Prepaid=Prepa+Prepaid;

    }else if (pd.payType ==3){
        var   Advance=pd.payMoney;
        AdvanceReceipt=Advance+AdvanceReceipt;

    }
}
    $(".incomes").val(incomes);
    $(".AdvanceReceipt").val(AdvanceReceipt);
    $(".Prepaid").val(Prepaid);
    $(".expenditures").val(expenditures);

},
});
}
    //会计获取数据
    function request() {
    var CustomerId= $("#search-input-fore").attr("i");
    var serviceUserId= $("#search-input-three").attr("i");
    var matterId= $("#search-input-fif").attr("i");

    var customerId=CustomerId;

    var stdata=  $("#startsDate").val();
    var endata=  $("#endsDate").val();
    if (stdata==""){
    startsDate=getPassYearFormatDate();
}else {
    startsDate=stdata;
}
    if (endata==""){
    endsDate=getNowFormatDate();
}else {

    endsDate=endata;
}

    var data = {};
    data["pageNo"] = 1;
    data["pageSize"] = 10000000;
    data["timeType"] = timeTypes;
    data["startDate"] = startDate;
    data["endDate"] = endDate;
    data["payType"] = '';
    data["customerId"] =customerId;
    data["matterId"] =matterId;
    data["serviceUserId"] =serviceUserId;
    data["order"] = '';
    data["column"] = '';
        sessionStorage.setItem("accounting", JSON.stringify(data));
    $.ajax({
    type: 'post',
    // url: 'js/pdlist1.json',
    url: path()+'/matter/matter/list',
    dataType: 'json',
    xhrFields: {
    withCredentials: true
},
    crossDomain: true,
    scriptCharset: 'utf-8',
    contentType: "application/json",
    data:JSON.stringify(data),
    success: function(data){


    var rel= data.data.data;
    // 收入
    var incomes=0;
    // 支出
    var expenditures=0;
    // 利润

    for (var i = 0; i < rel.length; i++) {

    var pd= rel[i]
    if (pd.payType == 1) {
    var   incom=pd.payMoney;
    incomes=incom+incomes;
}else if (pd.payType ==0){
    var   expen=pd.payMoney;
    expenditures=expen+expenditures;
}
}

    $(".incomes-one").val(incomes);
    $(".expenditures-one").val(expenditures);


},
});
}
    // 出纳搜索
    $("#search-for").click(function () {
    var stdata=  $("#startDate").val();
    var endata=  $("#endDate").val();
    var stdt=new Date(stdata.replace("-","/"));
    var etdt=new Date(endata.replace("-","/"));
    if(stdt>etdt) {
    alert("开始时间必须大于结束时间");
}else {
    if (cgetclass()){
        Cashier();

}else {
    alert("请选择搜索时间类型");
}
}




});
    // 会计搜索
    $("#search-for-one").click(function () {
    var stdata=  $("#startsDate").val();
    var endata=  $("#endsDate").val();
    var stdt=new Date(stdata.replace("-","/"));
    var etdt=new Date(endata.replace("-","/"));
    if(stdt>etdt) {
    alert("开始时间必须大于结束时间");
}else {
    if (kgetclass()){
        request();

}else {
    alert("请选择搜索时间类型");
}
}




});
    // 出纳重置
    $("#search-Reset").click(function () {
    if ($(".list-two input"). hasClass('but')) {
    $(".list-two input").removeClass('but');
}

    var stdata=  $("#startDate").val();
    var endata=  $("#endDate").val();
    var searchinput=  $("#search-input").val();
    var serviceUserId= $("#search-input-two").val();
    if (stdata!=""){
    $("#startDate").val("");
}
    if (endata!=""){
    $("#endDate").val("");
}
    if (searchinput!=""){
    $("#search-input").val("");
    $("#search-input").attr("i","");
}
    if (serviceUserId!=""){
    $("#search-input-two").val("");
    $("#search-input-two").attr("i","");
}





});
    // 会计重置
    $("#search-Reset-one").click(function () {
    if ($(".list-two input"). hasClass('but')) {
    $(".list-two input").removeClass('but');
}
    var stdata=  $("#startsDate").val();
    var endata=  $("#endsDate").val();
    var searchinput=  $("#search-input-fore").val();
    var serviceUserId= $("#search-input-three").val();
    var matterId= $("#search-input-fif").val();
    if (stdata!=""){
    $("#startsDate").val("");
}
    if (endata!=""){
    $("#endsDate").val("");
}
    if (searchinput!=""){
    $("#search-input-fore").val("");
    $("#search-input-fore").attr("i","");
}
    if (serviceUserId!=""){
    $("#search-input-three").val("");
    $("#search-input-three").attr("i","");
}
    if (matterId!=""){
    $("#search-input-fif").val("");
    $("#search-input-fif").attr("i","");
}





});
<!--出纳下拉-->
    $(function(){

        Cashier();
// 出纳客户下拉
    $.ajax({
    type: 'get',
    // url: 'js/pdlist1.json',
    url: path()+'/customer/customer',
    dataType: 'json',
    xhrFields: {
    withCredentials: true
},
    crossDomain: true,
    scriptCharset: 'utf-8',
    contentType: "application/json",
    success: function(data){
    for (var i=0; i<data.data.length;i++){
    var service=data.data[i];
    $("#customerId").append('<li ><a href="javaScript:" i="'+service.customerId+'">'+service.customerName+'</a></li>');
}

},
});
    // 出纳服务人员下拉
    $.ajax({
    type: 'get',
    url: path()+'/customer/customer',
    dataType: 'json',
    xhrFields: {
    withCredentials: true
},
    crossDomain: true,
    scriptCharset: 'utf-8',
    success : function (data) {
    sessionStorage.setItem("liandong", JSON.stringify(data.data));

    if (data.code == 2004) {
    alert("非法登陆");
    location.href = "../web/Backup/login.html";
} else {

    //接收返回的数据data
    var $a = data;
    var i = $a.data; 	//获取具体的参数
    $b = {warn: []};
    var arr = [];
    for (var x = 1; x < i.length; x++) {
    var serviceid = $a.data[x].servicerId;

    if (arr.indexOf(serviceid) == -1) {
    //  -1代表没有找到
    arr.push($a.data[x].servicerId);
    var sel = $a.data[x]//如果没有找到就把这个name放到arr里面，以便下次循环时用
    $b.warn.push(sel);
}
}
    var newarr = $b;
    num = $b.warn.length;       //去重后的数组
    for (j = 0; j < num; j++) {
    var pd = $b.warn[j];

    $("#servicerId").append('<li ><a href="javaScript:" i="'+pd.servicerId+'">'+pd.servicerName+'</a></li>');
}
}
},
});
});
<!--会计下拉-->
    $(function(){

    request();
// 会计客户
    $.ajax({
    type: 'get',
    // url: 'js/pdlist1.json',
    url: path()+'/customer/customer',
    dataType: 'json',
    xhrFields: {
    withCredentials: true
},
    crossDomain: true,
    scriptCharset: 'utf-8',
    contentType: "application/json",
    success: function(data){
    for (var i=0; i<data.data.length;i++){
    var service=data.data[i];
    $("#customerId-two").append('<li ><a href="javaScript:" i="'+service.customerId+'">'+service.customerName+'</a></li>');
}

},
});
    // 会计服务人员
    $.ajax({
    type: 'get',
    url: path()+'/customer/customer',
    dataType: 'json',
    xhrFields: {
    withCredentials: true
},
    crossDomain: true,
    scriptCharset: 'utf-8',
    success : function (data) {
    sessionStorage.setItem("accliandong", JSON.stringify(data.data));

    if (data.code == 2004) {
    alert("非法登陆");
    location.href = "../web/Backup/login.html";
} else {

    //接收返回的数据data
    var $a = data;
    var i = $a.data; 	//获取具体的参数
    $b = {warn: []};
    var arr = [];
    for (var x = 1; x < i.length; x++) {
    var serviceid = $a.data[x].servicerId;

    if (arr.indexOf(serviceid) == -1) {
    //  -1代表没有找到
    arr.push($a.data[x].servicerId);
    var sel = $a.data[x]//如果没有找到就把这个name放到arr里面，以便下次循环时用
    $b.warn.push(sel);
}
}
    var newarr = $b;
    num = $b.warn.length;       //去重后的数组
    for (j = 0; j < num; j++) {
    var pd = $b.warn[j];

    $("#servicerId-one").append('<li ><a href="javaScript:" i="'+pd.servicerId+'">'+pd.servicerName+'</a></li>');
}
}
},
});
    // 会计收支事项
    $.ajax({
    type: 'get',
    url: path()+'/matter/matter/find',
    dataType: 'json',
    xhrFields: {
    withCredentials: true
},
    crossDomain: true,
    scriptCharset: 'utf-8',
    success : function (data) {

    for (var j = 0; j < data.data.length; j++) {
    var pd=  data.data[j];
    $("#matterId").append('<li ><a href="javaScript:" i="'+pd.matterId+'">'+pd.matterName+'</a></li>');
}
},
});
});
<!--切换-->



