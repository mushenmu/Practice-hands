
$(function(){
    $(window).resize();
});
//js设置居中
$(window).resize(function() {

    $(".login-contain").css({
        left: ($(window).width() - $(".login-contain").outerWidth()) / 2,
        top: ($(window).height() - $(".login-contain").outerHeight()) / 2
    });
});
    $(function () {
        var imgUploadss=[];

         imgChange=function(obj1, obj2) {


            var previewImgList = document.querySelector('.z_photo');

            var liLen = previewImgList.getElementsByTagName('div').length;

            //获取点击的文本框
            var file = document.getElementById("file");
            //存放图片的父级元素
            var imgContainer = document.getElementsByClassName(obj1)[0];
            //获取的图片文件
            var fileList = file.files;
            var imgLen = fileList.length;
            var ImgLen = imgLen + liLen ;
            if(ImgLen > 8){
                alert("上传图片最大数量不能超过7张");
                return false;
            }
            //文本框的父级元素
            var input = document.getElementsByClassName(obj2)[0];
            var imgArr = [];
            //遍历获取到得图片文件

            for (var i = 0; i < fileList.length; i++) {
             var files=   file.files[i]
                var formData = new FormData();
                var file = fileList[i];

                var imgid;
                formData.append("file", file);
                $.ajax({
                    type: "POST",
                    url: path()+"/journalFile/upload/" + 1,
                    data: formData,
                    processData: false,//这个很有必要，不然不行
                    contentType: false,
                    dataType: "json",
                    mimeType: "multipart/form-data",
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function (data) {
                        if (data.code != 1000) {
                            alert("上传失败")
                        } else {
                            imgUploadss.push(data.data);
                            imgid=data.data;
                            alert("上传成功");
                            /*    $("#img").attr('src',);*/
                        }
                        ;

                    }


                });
                var imgUrl = window.URL.createObjectURL(files);
                imgArr.push(imgUrl);
                var img = document.createElement("img");
                img.setAttribute("src", imgArr[i]);
                var imgAdd = document.createElement("div");
                imgAdd.setAttribute("class", "z_addImg");
                imgAdd.setAttribute("i", imgid);
                imgAdd.appendChild(img);
                imgContainer.appendChild(imgAdd);
                //获取的图片文件
                // 上传图片

            };
            imgRemove();
        };


        function imgRemove() {
            ;
            var imgList = document.getElementsByClassName("z_addImg");
            var mask = document.getElementsByClassName("z_mask")[0];
            var cancel = document.getElementsByClassName("z_cancel")[0];
            var sure = document.getElementsByClassName("z_sure")[0];
            for (var j = 0; j < imgList.length; j++) {
                imgList[j].index = j;
                imgList[j].onclick = function() {
                    var t = this;
                    mask.style.display = "block";
                    cancel.onclick = function() {
                        mask.style.display = "none";
                    };
                    sure.onclick = function() {
                            debugger;
                        mask.style.display = "none";
                        var obj=   t.parentNode;
                      var imgId=  t. getAttribute("i");
                        $.ajax({
                     type: "GET",
                     url: path()+"/journalFile/delete/" + imgId,
                     dataType: "json",
                     contentType: "application/json",
                     xhrFields: {
                         withCredentials: true
                     },
                     crossDomain: true,
                     success: function (data) {
                         if (data.code != 1000) {
                             jqalert({
                                 title:'提示',
                                 content:'删除失败'
                             })
                         } else if (data.code == 1000) {
                             obj.removeChild(t);
                             jqalert({
                                 title:'提示',
                                 content:'删除成功'
                             })
                             /*    $("#img").attr('src',);*/
                         };
                     }


                 });

                    };

                }
            };
        };

        var matterId;
        $(".checkboxwrapper").on("click", "div", function () {
            var i = $(this).find("ul").attr("i");
            if (i == 1) {
                $(this).find("ul").removeClass("active");
                $(this).find("ul").attr("i", "");
            } else {
                $(this).find("ul").addClass("active");
                $(this).find("ul").attr("i", 1);
            }


            $(this).siblings("div").find("ul").removeClass("active");
            $(this).siblings("div").find("ul").attr("i", "");

            var address = $(this).find("ul").children("li").get(0);
            var matterIds = address.children[0];
            matterId = matterIds.getAttribute("i");


        });
        $('.checkboxwrapper div').unbind('clock');

        layui.use(['form', 'laydate'], function () {
            var form = layui.form
                , layer = layui.layer
                , layedit = layui.layedit
                , laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#date1' //指定元素
            });
            laydate.render({
                elem: '#date2' //指定元素
            });
            //监听提交
            form.on('submit(formDemo)', function (rel) {

                var data = {};
                var customerId = $("[name='customerId']").val();
                var payTime = $("[name='payTime']").val();
                var payType = $("[name='payType']").val();
                var payMoney = $("[name='payMoney']").val();
                var serviceUserId = $("[name='serviceUserId']").val();
                var payMethodId = $("[name='payMethodId']").val();
                var remark = $("[name='remark']").val();
                data["customerId"] = customerId;
                data["payTime"] = payTime;
                data["payType"] = payType;
                data["payMoney"] = payMoney;
                data["matterId"] = matterId;
                data["serviceUserId"] = serviceUserId;
                data["payMethodId"] = payMethodId;
                data["remark"] = remark;
                data["fileIds"] = imgUploadss;
                $.ajax({
                    type: "post",
                    url: path() + "/journal/user/journal",
                    xhrFields: {
                        withCredentials: true
                    },
                    crossDomain: true,
                    dataType: 'json',
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (data) {
                        console.log(data);
                        if (data.code != 1000) {
                            alert("新增失败");
                        } else {
                            window.location.reload();
                            alert("新增成功")
                        }
                    }
                });
                return false;
            });

            $.ajax({
                type: 'get',
                url: path() + '/customer/customer',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                scriptCharset: 'utf-8',
                success: function (data) {

                        //接收返回的数据data
                        var $a = data;
                        var i = $a.data; 	//获取具体的参数
                        $b = {warn: []};
                        var arr = [];
                        for (var x = 1; x < i.length; x++) {
                            var serviceid = $a.data[x].servicerId;

                            if (arr.indexOf(serviceid) == -1) {
                                //  -1代表没有找到
                                arr.push($a.data[x].servicerId);
                                var sel = $a.data[x]//如果没有找到就把这个name放到arr里面，以便下次循环时用
                                $b.warn.push(sel);
                            }
                        }
                        var newarr = $b;
                        num = $b.warn.length;       //去重后的数组
                        for (j = 0; j < num; j++) {
                            var pd = $b.warn[j];

                            $(".servicerId").append(' <option value="' + pd.servicerId + '">' + pd.servicerName + '</option>');
                        }
                        form.render('select');

                },
            });
            $.ajax({
                type: 'get',
                url: path() + '/customer/customer',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                scriptCharset: 'utf-8',
                success: function (data) {


                    for (var i = 0; i < data.data.length; i++) {
                        var pd = data.data[i];
                        /*  $("#demo_select1").append('<li i="'+pd.customerId+'" ><a href="javaScript:">'+pd.customerName+'</a></li>');*/
                        $(".customer").append(' <option value="' + pd.customerId + '">' + pd.customerName + '</option>');

                    }
                    form.render('select');

                }

            });

            $.ajax({
                type: 'get',
                url: path() + '/payMethodController/payMethod',
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                scriptCharset: 'utf-8',
                success: function (data) {


                    for (var i = 0; i < data.data.length; i++) {
                        var pd = data.data[i];
                        /*  $("#demo_select1").append('<li i="'+pd.customerId+'" ><a href="javaScript:">'+pd.customerName+'</a></li>');*/
                        $("#payMethod").append(' <option value="' + pd.payMethodId + '">' + pd.payMethodName + '</option>');

                    }
                    form.render('select');

                }

            });
        });


    })





    // JavaScript Document
/*
$("#top_sp").click(function () {

  var customerId=$("#demo_select1").val();
  var payTime=$("#demo_date1").val();
  var payType=$("#demo_select2").val();
  var payMoney=$("#payMoney").val();
  var serviceUserId=$("#demo_select3").val();
  var payMethodId=$("#demo_select4").val();
  var remark=$("#remark").val();

    $.ajax({
        method: "post",
        url:path()+ "/journal/user/journal",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        dataType:'json',
        contentType: "application/json",
        data:JSON.stringify(data),
        success: function (data) {
            console.log(data);
            if (data.code!= 1000) {
                alert("新增失败");
            } else {
                window.location.reload();
                alert("新增成功")
            }
        }
    });


})
*/

/*var startDate = document.getElementById("demo_date1");
var date = new Date();
var year = date.getFullYear();
var month = date.getMonth() + 1;
var strDate = date.getDate();
if (month >= 1 && month <= 9) {
    month = "0" + month;
}
if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
}
var currentdate = year +"-"+ month +"-"+ strDate;

startDate.value = currentdate;*/

$("#button_tp").click(function () {
    window.location.href="add-ctures.html"
})