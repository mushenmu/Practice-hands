/**
 * Created by an.han on 14-2-20.
 */

function wonde() {


    var list = document.getElementById('dataList');
    var boxs = list.children;
    var timer;

    //格式化日期
    function formateDate(date) {
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        var d = date.getDate();
        var h = date.getHours();
        var mi = date.getMinutes();
        m = m > 9 ? m : '0' + m;
        return y + '-' + m + '-' + d + ' ' + h + ':' + mi;
    }

    //删除节点
    function removeNode(node) {
        node.parentNode.removeChild(node);
    }



    /**
     * 发评论
     * @param box 每个分享的div容器
     * @param el 点击的元素
     */
    function reply(box, el) {

        var commentList = box.children[0];/*.getElementById("matterId");*/;
        var gdnum = commentList.children[0];/*.getElementById("matterId");*/;
        var gdnums = gdnum.children[0];/*.getElementById("matterId");*/;
        var gdnumss = gdnums.children[1];/*.getElementById("matterId");*/;
        var matterId = gdnumss.children[0];
        var matterIsd=  matterId. getAttribute("i")
        var textarea = box.getElementsByClassName('comment')[0];
        var data = {} ;
        data["content"] = textarea.value;
        data["matterId"] = matterIsd;
        $.ajax({
            url: path()+ "/matterChangeRecord/comment",
            contentType: "application/json;charset=utf-8",
            type: "post",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            data:JSON.stringify(data),
            success: function (data) {
                console.log(data)
                if (data.code!=1000){
                    alert("评论失败");
                }else{
                    alert("评论成功");
                    window.location.reload();
                    /*commentBox.className = 'comment-box clearfix';
                    commentBox.setAttribute('user', 'self');
                    commentBox.innerHTML =
                        '<div class="comment-content">' +
                        '<p class="comment-text"><span class="user">我：</span>' + textarea.value + '</p>' +
                        '<p class="comment-time">' +
                        formateDate(new Date()) +
                        '<a href="javascript:;" class="comment-operate">删除</a>' +
                        '</p>' +
                        '</div>'
                    commentList.appendChild(commentBox);
                    textarea.value = '';
                    textarea.onblur();*/
                }

            }
        });
    }
    /**
     * 通过评论
     * @param box 每个分享的div容器
     * @param el 点击的元素
     */
    function by(box, el) {
        debugger
        var commentList = box.children[0];/*.getElementById("matterId");*/;
        var gdnum = commentList.children[1];/*.getElementById("matterId");*/;
        var matterId = gdnum.children[0];
        var matterIsd=  matterId. getAttribute("i")/*.getElementById("matterId");*/;
        var textarea = commentList.parentNode.getElementsByClassName('opinion')[0];
        var textarea= textarea.value;
        $.ajax({
            type: "POST",
            url: path()+ "/matter/pass/matter/"+matterIsd,
            contentType: "application/json;charset=utf-8",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            data:JSON.stringify(textarea),
            success: function (data) {
                console.log(data)
                if (data.code!=1000){
                    alert("评论失败");
                }else{
                    alert("评论成功");
                    window.location.reload();
                }

            }
        });
    }
    /**
     * 拒绝评论
     * @param box 每个分享的div容器
     * @param el 点击的元素
     */
    function Refuse(box, el) {
        debugger;
        var commentList = box.children[0];/*.getElementById("matterId");*/;
        var gdnum = commentList.children[1];/*.getElementById("matterId");*/;
        var matterId = gdnum.children[0];
      var matterIsd=  matterId. getAttribute("i")/*.getElementById("matterId");*/;
        var textarea = commentList.parentNode.getElementsByClassName('opinion')[0];
       var textarea= textarea.value;
        $.ajax({
            type: "POST",
            url: path()+ "/matter/stop/matter/"+matterIsd,
            contentType: "application/json;charset=utf-8",

            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            data:JSON.stringify(textarea),
            success: function (data) {
                console.log(data)
                if (data.code!=1000){
                    alert("评论失败");
                }else{
                    alert("评论成功");
                    window.location.reload();
                    /* commentBox.className = 'comment-box clearfix';
                     commentBox.setAttribute('user', 'self');
                     commentBox.innerHTML =
                         '<div class="comment-content">' +
                         '<p class="comment-text"><span class="user">我拒绝：</span>' + textarea.value + '</p>' +
                         '<p class="comment-time">' +
                         formateDate(new Date()) +
                         '<a href="javascript:;" class="comment-operate">删除</a>' +
                         '</p>' +
                         '</div>'
                     commentList.appendChild(commentBox);
                     textarea.value = '';
                     textarea.onblur();*/
                }

            }
        });
    }

    /**
     * 操作留言
     * @param el 点击的元素
     */
    function operate(el) {
        var commentBox = el.parentNode.parentNode.parentNode;
        var box = commentBox.parentNode.parentNode.parentNode;
        var txt = el.innerHTML;
        var user = commentBox.getElementsByClassName('user')[0].innerHTML;
        var textarea = box.getElementsByClassName('comment')[0];
        if (txt == '回复') {
            textarea.focus();
            textarea.value = '回复' + user;
            textarea.onkeyup();
        }
        else {
            removeNode(commentBox);
        }
    }

    //把事件代理到每条分享div容器
    for (var i = 0; i < boxs.length; i++) {

        //点击
        boxs[i].onclick = function (e) {


            e = e || window.event;
            var el = e.srcElement;
            switch (el.className) {


                //回复按钮蓝
                case 'btn':
                    reply(el.parentNode.parentNode.parentNode, el);
                    break;

                //回复按钮灰
                case 'btn btn-off':
                    clearTimeout(timer);
                    break;

                //通过按钮
                case 'by':
                    by(el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //通过按钮
                case 'by Re':
                    by(el.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //拒绝按钮
                case 'Refuse':
                    debugger
                    Refuse(el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //拒绝按钮
                case 'Refuse Re':
                    Refuse(el.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //通过按钮
                case 'bys':
                    by(el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //通过按钮
                case 'bys Re':
                    by(el.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //拒绝按钮
                case 'Refuses':
                    debugger
                    Refuse(el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //拒绝按钮
                case 'Refuses Re':
                    Refuse(el.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //通过
                case 'icon-fuxuan':
                    tongguo(el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;
                //拒绝
                case 'icon-jujue1':
                    jujue(el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode, el);
                    break;


                //操作留言
                case 'comment-operate':
                    operate(el);
                    break;
            }
        }

        //评论
        var textArea = boxs[i].getElementsByClassName('comment')[0];



        //评论获取焦点
        textArea.onfocus = function () {

            this.parentNode.className = 'text-box text-box-on';
            this.value = this.value == '评论…' ? '' : this.value;
            this.onkeyup();
        };


        //评论失去焦点
        textArea.onblur = function () {
            var me = this;
            var val = me.value;
            if (val == '') {
                timer = setTimeout(function () {
                    me.value = '评论…';
                    me.parentNode.className = 'text-box';
                }, 200);
            }
        }

        //评论按键事件
        textArea.onkeyup = function () {

            var val = this.value;
            var len = val.length;
            var els = this.parentNode.children;
            var btn = els[1];
            var word = els[2];
            if (len <=0 || len > 140) {
                btn.className = 'btn btn-off';
            }
            else {
                btn.className = 'btn';
            }
            word.innerHTML = len + '/140';
        }

    }


    $('#img').FlyZommImg({
        rollSpeed: 200,//切换速度
        miscellaneous: true,//是否显示底部辅助按钮
        closeBtn: true,//是否打开右上角关闭按钮
        hideClass: 'hide',//不需要显示预览的 class
        imgQuality: 'thumb',//图片质量类型  thumb 缩略图  original 默认原图
        slitherCallback: function (direction, DOM) {//左滑动回调 两个参数 第一个动向 'left,firstClick,close' 第二个 当前操作DOM
//                   console.log(direction,DOM);
        }
    });
}


